# simple_photo_importer

Simple Photo Importer (spi)

A small bash script with a YAD driven gui to copy files. It's meant to import photos or video files from a source like a SD-Card.

It creates a basic destination path structure: year/month/day
Files are renamed, the new file name is created by using cksum to identify duplicates. The file extension is preserved.
Duplicate files could be ignored or copied to a special path.

Photo creation time is identified by exiftool or date.
Dependencies: yad, exiftool