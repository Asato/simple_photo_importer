#!/bin/bash
# Asato/Unchained
# GPL3 Licenced
# 19. Oct 2021
# check dependencies: yad, exiftool (in debian e.g. apt-get install libimage-exiftool-perl)

# note: yad will only return field values if buttons with even IDs are pressed 
# 14. Jan 2025: changed from CreateDate to DateTimeOriginal

error=1;
while [ $error == 1 ]; do
	action=$(yad\
		--title="Simple Photo Importer"\
		--borders=20\
		--center\
		--text-align=center\
		--text="Choose source and destination directory"\
		--form\
		--field="Source :DIR"\
		--field="Destination :DIR"\
		--field="Copy duplicate files:CHK"\
		--button="Quit:1"\
		--button="Run:2")
	
	# exit status of yad is defined by button IDs
	result=$?
	
	# Quit button
	if [[ $result -eq 1 ]]; then
		exit
	fi
	
	# cut: -d separator, f<num> the field
	full_source_dir=$(echo $action|cut -d'|' -f1)
	full_dest_dir=$(echo $action|cut -d'|' -f2)
	copy_duplicates=$(echo $action|cut -d'|' -f3)
	
	# test
	if [ "$full_source_dir" == "$full_dest_dir" ]; then
		catch=$(yad\
		 --title="Error"\
		 --borders=20\
		 --image="dialog-error"\
		 --text="Source and destination path must be different!"\
		 --button=gtk-ok)
		 
	else
		error=0
	fi
done

# for the stats
number_of_files=0
number_of_files_copied=0
number_of_duplicates=0

# count files and show progress
tmpfile=$(mktemp)
find "$full_source_dir" -type f|
{
	while read myfiles; do
		((number_of_files++))
		# send to yad progress dialog
		echo "# Found $number_of_files files"
	done
	echo "$number_of_files">"$tmpfile"
	# close window if ready - send "100%"
	echo "100"
}|yad\
	--title="Simple Photo Importer"\
	--borders=20\
	--center\
	--text-align=center\
	--text="Counting files ..."\
	--progress\
	--auto-close\
	--pulsate\
	--button="Stop:1"

result=$?

# Stop button hit?
if [[ $result -eq 1 ]]; then
	rm "$tmpfile"
	exit
fi	

number_of_files="$(cat "$tmpfile")"
rm "$tmpfile"

count=0
find "$full_source_dir" -type f|
# command grouping for counting ...
{
	while read myfiles; do
		# for progressbar in yad
		((count++))
		perc=$((100*count/number_of_files))
		echo "$perc"
		echo "# Processing file $count/$number_of_files ($perc %)"
		
		# check for file extension ... $ -> last occurrence
		file_extension=""
		if [[ "$myfiles" =~ \.([^\.]*)$ ]]; then
			# to lowercase ...
			file_extension=$(echo "${BASH_REMATCH[0]}" | tr [:upper:] [:lower:])
		fi

		### calculate cksum as new file name
		
		# cksum output: cksum, filesize, filename
		cksum_output_raw="$(cksum "$myfiles")"
		
		# interprete as array
		cksum_output_array=($cksum_output_raw)
		
		# first entry is chksum
		file_checksum=${cksum_output_array[0]}

		### get date of file for directory structure
		### even though there should be no difference between CreateDate and DateTimeOriginal it seems sometimes that the latter is more reliable
		file_information=$(exiftool "$myfiles" -DateTimeOriginal -S -t -d datetime=%Y:%m:%d)
		
		# check for datetime tag ...
		if ! [[ "$file_information" =~ datetime=([0-9]{4}):([0-9]{2}):([0-9]{2}) ]]; then
			# take file date ...
			file_information=$(date -r "$myfiles" +datetime=%Y:%m:%d)	
			
			# match again ...
			[[ "$file_information" =~ datetime=([0-9]{4}):([0-9]{2}):([0-9]{2}) ]]
		fi
		
		dest_dir="$full_dest_dir/${BASH_REMATCH[1]}/${BASH_REMATCH[2]}/"
		dest_duplicates_dir="$full_dest_dir/duplicates/${BASH_REMATCH[1]}/${BASH_REMATCH[2]}/"

		dest_filename="$file_checksum$file_extension"
		dest_full_name="$dest_dir/$dest_filename"
		dest_full_duplicate_name="$dest_duplicates_dir/$dest_filename"

		### try to copy file with new name to new dest
		
		# check for file ...	
		if ! [ -f "$dest_full_name" ]; then

			# check if directory exists ...
			if ! [ -d "$dest_dir" ]; then
			
				# create destination directory ...
				error="$(mkdir -p "$dest_dir")"
				
				# failed?
				if [ "$error" != "" ]; then
						catch=$(yad\
							--title="Error"\
							--borders=20\
							--image="dialog-error"\
							--text="An error occured creating $dest_dir: $error"\
							--button=gtk-ok)
						exit
				fi
			else
				: # echo "# Directory $dest_dir exists"
			fi

			# file doesn't exists, so copy ...
			# echo "# Copying file $myfiles to $dest_full_name"
			error="$(cp -p "$myfiles" "$dest_full_name")"
			if [ "$error" != "" ]; then
				: # echo "# ERROR: $error"
			else
				# count copied files ...
				((number_of_files_copied++))
			fi
			
		else
			# file exists
			# echo "# File $myfiles exists as $dest_full_name"
			
			# count duplicates
			((number_of_duplicates++))
			
			## copy to duplicates dir for manual check 
			if [ "$copy_duplicates" == "TRUE" ]; then
			
				# echo "# copy to duplicates dir"

				if ! [ -f "$dest_full_duplicate_name" ]; then
					# test for duplicates dir
					if ! [ -d "$dest_duplicates_dir" ]; then
						error="$(mkdir -p "$dest_duplicates_dir")"

						# failed?
						if [ "$error" != "" ]; then
							catch=$(yad\
								--title="Error"\
								--borders=20\
								--image="dialog-error"\
								--text="An error occured creating $dest_duplicates_dir: $error"\
								--button=gtk-ok)
							exit
						fi
					fi
				
					error="$(cp -p "$myfiles" "$dest_full_duplicate_name")"
					if [ "$error" != "" ]; then
						: # echo "# ERROR: $error"
					else
						# count copied files ...
						((number_of_files_copied++))
					fi
				else
					: # echo "# File exists as duplicate"
				fi
			fi
		fi

	done
	
# final stats ..
	echo "# $number_of_files files found, $number_of_duplicates duplicates, $number_of_files_copied copied"

}|yad\
	--title="Simple Photo Importer"\
	--borders=20\
	--center\
	--text-align=center\
	--text="Importing Files"\
	--progress\
	--percentage=0\
	--button="Done:1"
